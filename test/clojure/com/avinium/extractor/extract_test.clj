(ns com.avinium.extractor.core-test
  (:gen-class)
  (:require [clojure.test :refer :all]
            [clojure.data :refer :all]
            [clojure.java.io :as io]
            [com.avinium.extractor.core :refer :all]))

(deftest test-extract-xml
  "Parse a mock wikipedia file on the local filesystem into its component inputstreams"
  (extract (.toString (io/resource "test/wiki.xml"))))

(deftest test-extract-html
  "Parse an HTML file on the local filesystem into an inputstream of its body only"
  (extract (.toString (io/resource "test/austlii.html"))))

(deftest test-extract-xml
  "Parse a mock wikipedia file on HDFS  into its component inputstreams"
  (extract "hdfs://localhost:8020/wiki.xml"))

(extract (.toString (io/resource "test/austlii.html")))
