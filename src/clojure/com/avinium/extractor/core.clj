(ns com.avinium.extractor.core
  (:gen-class)
  (:require [clojure.core :as core]
            [clojure.string :as str]
            [clojure.tools.logging :as log]
            [clojure.java.io :as io]
            [net.cgrand.enlive-html :as html]
            [clojure.data.xml :as data.xml])
  (:import org.apache.hadoop.fs.FileSystem)
  (:import org.apache.hadoop.fs.Path)
  (:import org.apache.hadoop.conf.Configuration)
  (:import java.io.ByteArrayInputStream))

(def formats
  ["csv"
 "eml"
 "doc"
 "docx"
 "html"
 "pdf"
 "rtf"
 "txt"
 "xls"
 "xlsx"
 "zip"])

(defn getExtension [pString]
  (let
    [split (str/split pString #"\.")]
    (if (> (count split) 1)
      (nth split (dec (count split))))))

(defn to-stream [filePath]
  (do
    (println "Splitting path " filePath " into component streams")
    (let [path (Path. filePath)
          fs   (.getFileSystem path (new Configuration))
          is   (.open fs path)]
    is)))

(defn xml
  [stream]
  (let [rdr (io/reader stream)
         parsed (data.xml/parse rdr)
         rootTag (:tag parsed)]
    (if (= :mediawiki rootTag)
        (->>   parsed
               :content
               (filter #(= (:tag %) :page))
               (map #(:content %))
               (map (fn [page] (first (filter (fn [child] (= (:tag child) :revision)) page))))
               (map #(:content %))
               (mapcat (fn [firstRev] (filter #(= (:tag %) :text) firstRev)))
               (mapcat #(:content %))
               (map #(java.io.ByteArrayInputStream. (.getBytes %)))))))

(defn html
  [stream]
  (with-open
    [rdr (io/reader stream)]
        (->>   (doall (map #(str/split % #"<[^>]+>") (line-seq rdr)))
               (apply str)
               (.getBytes)
               (java.io.ByteArrayInputStream.)
               (vector))))

(defn extract
  [path]
  (let
    [extension (getExtension (.toString path))
     stream (to-stream path)]
    (case extension
      "html" (html stream)
      "txt" [stream]
      "xml" (xml stream)
      nil)))
