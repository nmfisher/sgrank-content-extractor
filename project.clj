(defproject com.avinium/extractor "0.0.2-SNAPSHOT"
  :description "Splits a file path (file:// or hdfs://) into one or more InputStreams for parsing with sgrank-dictionary. Currently only Wikipedia XML and txt supported."
  :dependencies [
                  [org.clojure/clojure "1.8.0"]
                  [org.clojure/tools.cli "0.3.5"]
                  [org.clojure/tools.logging "0.3.1"]
                  [org.clojure/data.zip "0.1.1"]
                  [org.clojure/data.xml "0.0.8"]
                  [enlive "1.1.6"]
                  [org.apache.hadoop/hadoop-client "2.7.1"]]
  :javac-options ["-target" "1.6" "-source" "1.6" "-Xlint:-options"]
  :source-paths ["src/clojure"]
  :test-paths ["test/clojure"]
  :main com.avinium.extractor.core
  :aot :all)
