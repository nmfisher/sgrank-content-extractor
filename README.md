# clojure.main

Splits a file path (file:// or hdfs://) into one or more InputStreams for parsing with sgrank-dictionary. Currently only Wikipedia XML and txt supported.

## Usage

(com.avinium/extractor/extractor [path])
